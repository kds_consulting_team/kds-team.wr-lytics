'''
Template Component main class.

'''

import csv
import glob
import json
import logging
import os
import sys
from enum import Enum

import requests
from kbc.env_handler import KBCEnvHandler
from nested_lookup import nested_lookup
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

LYTICS_API_URL = 'https://api.lytics.io'
# configuration variables
KEY_USER_PARS = 'user_parameters'

KEY_PATH = 'path'
KEY_MODE = 'mode'
KEY_METHOD = 'method'

# additional request params
KEY_HEADERS = 'headers'
KEY_ADDITIONAL_PARS = 'additional_requests_pars'
STATUS_FORCELIST = (500, 501, 502, 503)
MAX_RETRIES = 3

# DYNAMIC PARS

KEY_TOKEN = '#token'
KEY_STREAM = 'stream'
KEY_DRYRUN = 'dryrun'
KEY_FILENAME = 'filename'
KEY_TIMESTAMP_FIELD = 'timestamp_field'

KEY_MODEL = 'model'
KEY_LOAD_TYPE = 'load_type'
KEY_MODEL_TYPE = 'model_type'
KEY_MODEL_MAPPING = 'model_mapping'
KEY_SRC_COL = 'src_col'
KEY_DST_NAME = 'dst_name'
KEY_SHORT_DESC = 'short_desc'
KEY_TYPE = 'type'
KEY_IS_BY = 'is_by'

# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = [KEY_TOKEN, KEY_STREAM]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'


class LyticsTypes(Enum):
    INT = "INT",
    NUMBER = "NUMBER",
    STRING = "STRING",
    DATE = "DATE",
    EMAIL = "EMAIL",
    EMAIL_DOMAIN = "EMAIL_DOMAIN",
    CUSTOM_LQL = "CUSTOM_LQL"


class LoadTypes(Enum):
    UPDATE_MODEL_AND_DATA = "UPDATE_MODEL_AND_DATA",
    MODEL_ONLY = "MODEL_ONLY",
    DATA_ONLY = "DATA_ONLY"


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config(MANDATORY_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

        # intialize instance parameteres
        self.user_functions = Component.UserFunctions(self)
        # set constants
        self.cfg_params[KEY_PATH] = 'https://bulk.lytics.io/collect/bulk/' + self.cfg_params[KEY_STREAM]
        self.cfg_params[KEY_METHOD] = 'POST'
        if not self.cfg_params.get(KEY_TIMESTAMP_FIELD):
            timestamp_field = None
        else:
            timestamp_field = self.cfg_params.get(KEY_TIMESTAMP_FIELD)

        self.cfg_params[KEY_ADDITIONAL_PARS] = [
            {
                "key": "params",
                "value": {
                    "dryrun": self.cfg_params.get(KEY_DRYRUN, False),
                    "filename": self.cfg_params.get(KEY_FILENAME, None),
                    "timestamp_field": timestamp_field
                }
            }
        ]
        self.cfg_params[KEY_HEADERS] = [{
            "key": "Authorization",
            "value": self.cfg_params[KEY_TOKEN]
        }, {
            "key": "Content-Type",
            "value": "application/csv"
        }]

        self.dry_run = self.cfg_params.get(KEY_DRYRUN)
        self.load_type = self.cfg_params[KEY_MODEL].get(KEY_LOAD_TYPE, LoadTypes.UPDATE_MODEL_AND_DATA.name)
        self._auth_token = self.cfg_params[KEY_TOKEN]

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        headers_cfg = params.get(KEY_HEADERS, {})
        additional_params_cfg = params.get(KEY_ADDITIONAL_PARS, [])

        headers_cfg = self._fill_in_user_parameters(headers_cfg, self.cfg_params.get(KEY_USER_PARS, []))
        additional_params_cfg = self._fill_in_user_parameters(additional_params_cfg,
                                                              self.cfg_params.get(KEY_USER_PARS, []))

        logging.info('Processing input mapping.')
        in_tables = glob.glob(self.tables_in_path + "/*[!.manifest]")
        if len(in_tables) == 0:
            logging.exception('There is no table specified on the input mapping! You must provide one input table!')
            exit(1)
        elif len(in_tables) > 1:
            logging.error(
                'There is more than one table specified on the input mapping! You must provide single input table!!')
            exit(1)

        in_table = in_tables[0]
        # validate model
        model_mapping = params.get(KEY_MODEL, {}).get(KEY_MODEL_MAPPING)

        if model_mapping:
            self._validate_model_mapping(in_table, model_mapping)

        logging.info("Building parameters..")
        # build headers
        headers = {}
        if params.get(KEY_HEADERS):
            for h in headers_cfg:
                headers[h["key"]] = h["value"]

        # build additional parameters
        additional_params = {}
        if additional_params_cfg:
            for h in additional_params_cfg:
                # convert boolean
                val = h["value"]
                if isinstance(val, str) and val.lower() in ['false', 'true']:
                    val = val.lower() in ['true']
                additional_params[h["key"]] = val

        additional_params['headers'] = headers

        if self.dry_run:
            logging.warning("Running in DRY RUN mode.")

        logging.info(f"Running in Load Mode: {self.load_type}")

        if self.load_type in (LoadTypes.DATA_ONLY.name, LoadTypes.UPDATE_MODEL_AND_DATA.name):
            logging.info(f"Pushing data to the stream: {params[KEY_STREAM]}")
            self.send_binary_data(params[KEY_PATH], additional_params, in_table)

        logging.info("Processing model...")
        model_mapping = params.get(KEY_MODEL, {}).get(KEY_MODEL_MAPPING)
        model_type = params.get(KEY_MODEL, {}).get(KEY_MODEL_TYPE)
        if model_mapping:
            # build query
            query = self._build_lql(model_mapping, params[KEY_STREAM], model_type)
            query_path = os.path.join(self.files_in_path, 'query.lql')
            with open(query_path, 'wt') as out:
                out.write(query)

            if self.dry_run:
                try:
                    logging.warning("Running dry run of the LQL / data upload.")
                    self.query_test_evaluation(query_path, in_table, model_mapping)
                except Exception as e:
                    logging.error(f'Query test failed {e}')
                    exit(1)
            elif self.load_type in (LoadTypes.MODEL_ONLY.name, LoadTypes.UPDATE_MODEL_AND_DATA.name):
                try:
                    logging.info("Upserting LQL query into Lytics.")
                    self.upload_lql_query(query_path)
                except Exception as e:
                    logging.error(f'Failed to send the query {e}')
                    exit(1)

        logging.info("Writer finished")

    def query_test_evaluation(self, query_path, in_table, model_mapping):
        # build sample data
        sample_data = {}
        with open(in_table, mode='r') as input_m:
            reader = csv.DictReader(input_m)
            sample_data = [line for i, line in enumerate(reader) if i <= 5]

        headers = self._get_auth_header()
        headers['Content-Type'] = 'text/plain'
        for line in sample_data:
            params = {'headers': headers, "params": line}
            res = self.send_binary_data(LYTICS_API_URL + '/api/query/_test', params, query_path)
            logging.warning(json.dumps(res.json()["data"]))

    def upload_lql_query(self, query_path):
        headers = self._get_auth_header()
        headers['Content-Type'] = 'text/plain'
        params = {'headers': headers}
        self.send_binary_data(LYTICS_API_URL + '/api/query', params, query_path)

    def _get_auth_header(self):
        return {'Authorization': self._auth_token}

    def send_request(self, url, additional_params, method='POST'):
        s = requests.Session()

        r = self._requests_retry_session(session=s).request(method, url, **additional_params)
        if r.status_code > 299:
            raise Exception(f"Failed to send request to {url} with error {r.json()['message']}")
        return r

    def send_binary_data(self, url, additional_request_params, in_table):
        in_path = in_table

        with open(in_path, mode='rb') as in_file:
            additional_request_params['data'] = in_file
            return self.send_request(url, additional_request_params)

    def _requests_retry_session(self, session=None):
        session = session or requests.Session()
        retry = Retry(
            total=MAX_RETRIES,
            read=MAX_RETRIES,
            connect=MAX_RETRIES,
            backoff_factor=0.5,
            status_forcelist=STATUS_FORCELIST,
            method_whitelist=('GET', 'POST', 'PATCH', 'UPDATE')
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def _fill_in_user_parameters(self, conf_objects, user_param):
        # convert to string minified
        steps_string = json.dumps(conf_objects, separators=(',', ':'))
        # dirty and ugly replace
        for key in user_param:
            if isinstance(user_param[key], dict):
                # in case the parameter is function, validate, execute and replace value with result
                user_param[key] = self._perform_custom_function(key, user_param[key])

            lookup_str = '{"attr":"' + key + '"}'
            steps_string = steps_string.replace(lookup_str, '"' + str(user_param[key]) + '"')
        new_steps = json.loads(steps_string)
        non_matched = nested_lookup('attr', new_steps)

        if non_matched:
            raise ValueError(
                'Some user attributes [{}] specified in configuration '
                'are not present in "user_parameters" field.'.format(non_matched))
        return new_steps

    def _perform_custom_function(self, key, function_cfg):
        if not function_cfg.get('function'):
            raise ValueError(
                F'The user parameter {key} value is object and is not a valid function object: {function_cfg}')
        new_args = []
        for arg in function_cfg.get('args'):
            if isinstance(arg, dict):
                arg = self._perform_custom_function(key, arg)
            new_args.append(arg)
        function_cfg['args'] = new_args

        return self.user_functions.execute_function(function_cfg['function'], *function_cfg.get('args'))

    class UserFunctions:
        """
        Custom function to be used in configruation
        """

        def __init__(self, component: KBCEnvHandler):
            # get access to the environment
            self.kbc_env = component

        def validate_function_name(self, function_name):
            supp_functions = self.get_supported_functions()
            if function_name not in self.get_supported_functions():
                raise ValueError(
                    F"Specified user function [{function_name}] is not supported! "
                    F"Supported functions are {supp_functions}")

        @staticmethod
        def get_supported_functions():
            return [method_name for method_name in dir(Component.UserFunctions)
                    if callable(getattr(Component.UserFunctions, method_name)) and not method_name.startswith('__')]

        def execute_function(self, function_name, *pars):
            self.validate_function_name(function_name)
            return getattr(Component.UserFunctions, function_name)(self, *pars)

        def string_to_date(self, date_string, date_format='%Y-%m-%d'):
            start_date, end_date = self.kbc_env.get_date_period_converted(date_string, date_string)
            return start_date.strftime(date_format)

        def concat(self, *args):
            return ''.join(args)

    def _validate_model_mapping(self, in_table, model_mapping):
        with open(in_table, mode='r') as input_m:
            reader = csv.DictReader(input_m)
            header = reader.fieldnames
            missing_cols = []
            for m in model_mapping:
                if m[KEY_TYPE] != LyticsTypes.CUSTOM_LQL.name and m[KEY_SRC_COL] not in header:
                    missing_cols.append(m[KEY_SRC_COL])
        errmsgs = []
        if missing_cols:
            errmsgs.append(f'Some source columns specified in mapping do not exist in the source! {missing_cols}')

        if errmsgs:
            raise ValueError(f'Input mapping validation failed: {",".join(errmsgs)}')

    def _build_lql(self, model_mapping, stream_name, model_type):
        """

        :param model_mapping:
        :return:
        """
        query = 'SELECT \n'
        # at this point I know it exists
        by_columns = []
        for ix, m in enumerate(model_mapping):
            if m[KEY_IS_BY]:
                by_columns.append(m[KEY_DST_NAME])

            source = m[KEY_SRC_COL]
            data_type = m[KEY_TYPE]
            kind = ''
            # custom LQL
            if data_type == LyticsTypes.CUSTOM_LQL.name:
                # extract type from src
                kind_spl = source.lower().split(' kind ')
                if len(kind_spl) > 1:
                    data_type = kind_spl[1]
                else:
                    data_type = None
                source = source[0:len(kind_spl[0])]

            if data_type:
                kind = f'KIND {data_type}'

            if data_type == LyticsTypes.EMAIL.name:
                source = f'email({m[KEY_SRC_COL]})'
                kind = ''

            if data_type == LyticsTypes.EMAIL_DOMAIN.name:
                source = f'emaildomain({m[KEY_SRC_COL]})'
                kind = ''

            line = f'   {source}  AS {m[KEY_DST_NAME]}   SHORTDESC "{m[KEY_SHORT_DESC]}"  {kind} \n'
            if ix > 0:
                line = ',' + line
            query += line

        from_cl = f"FROM {stream_name}\n" \
                  f"INTO {model_type} BY {' OR '.join(by_columns)}\n" \
                  f"ALIAS {stream_name}"

        query += from_cl

        return query


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except requests.HTTPError as er:
        if er.response.status_code == 401:
            er.args = (er.args[0] + ' Check your API key!',)
        logging.exception(er)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(1)
