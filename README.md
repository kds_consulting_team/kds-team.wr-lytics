# KBC Lytics Writer

Write data to custom Lytics streams. Allows sending large volumes of data using gzipped stream and dry run mode 
to verify your data.

**Table of contents:**  
  
[TOC]


# Functionality notes

Send events to your custom event stream. Takes a **single table** on the input, containing the data.

- The data is sent as gzipped csv via the [bulk upload API](https://learn.lytics.com/documentation/developer/api-docs/data-upload#bulk-csv-upload-bulk-csv-upload-post) 
so large volumes are supported. 
- The event stream is automatically created if does not exist.
- Creates a LQL query based on parameters with alias matching the Stream Name.
- May be executed multiple times

# Configuration

## API Token

You Lytics API token

## Stream

Name of the event stream. Note that ifn the event stream does not exist already, it is created automatically.

## Dry run

Dry run to verify the data. Only do a dry-run to see if the data is formatted correctly and the query is structured properly. 

**NOTE:**  The data upload and query dry run is executed regardless the `Model -> Load Type` setup. 
When in dry run mode a sample data row is sent to the `query/test` endpoint and the result printed in the 
job log to verify it behaves properly. The job log will contain record like this:

```
Running dry run of the LQL / data upload.
{"_created": "2020-09-22T13:33:43.741754223Z", "_modified": "2020-09-22T13:33:43.741754223Z", "email": "david@keboola.com", "last_order_date": "2020-01-01T00:00:00Z", "name": "keboola.com", "order_value": "150", "test": "a"}
```




## File name

Name of the uploaded file - Just for record-keeping in the Lytics event - stream.

## Timestamp Field

Optional parameter. The name of the column or field in file that contains event timestamp.

## Model

Lytics Query (Model) definition. Query with the same alias as the `Stream name` will be created or updated according to the specified mapping.

No model is created if the mapping is empty.

### Model Type

Type of the table you want to update. By default `user`, but you may want to update also `content` or other.

### Load Type

The load type defines what actions will be performed on each exectuion:

- **Update Model and Data** - uploads model (LQL query) and loads data each time
- **Update Model Only** - Only updates the LQL query
- **Update Data Only** - Only loads data to the data stream, the LQL query stays intact

### Model Mapping

Mapping of source column tables to the destination table columns

- **Destination Column** - must not contain blank characters
- **Type** - if set to `Email`, `email()` function will be applied to the field
- **Is Identifier** - If set to true, the field will be used in the `BY` clause. May be set only on one column.

Example of generated query:

```sql
SELECT 
   email(email)  AS email   SHORTDESC ""   
,   price  AS Order_Value   SHORTDESC ""  KIND INT 
,   date  AS Last_Order_Date    SHORTDESC "last order"  KIND DATE 
,   name  AS Name   SHORTDESC "user name"  KIND STRING 
,   test  AS Test   SHORTDESC "test"  KIND STRING 
FROM custom-keboola-testing-model
INTO user BY email
ALIAS custom-keboola-testing-model
```

#### Data Types

Apart from standard primitive datatypes, additional are supported:

- **EMAIL** - `email()` function will be applied to the field
- **EMAIL_DOMAIN** - `emaildomain()` function will be applied to the field

##### **CUSTOM_LQL**

Specify custom LQL (function) in the `Column` field instead of the source table column name. This allows to apply some
 additional LQL functions such as `Map` datatypes. The name of the actual existing column form the source table must be included.
 It is possible to define the KIND as well.
 
 **Examples**:
 
 `map(key1, todate(date_field)) KIND map[string]time` - `date_field` is a column in the input table
 `domain(url)`- `url` is a column in the input table


# Development

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in the docker-compose file:

```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone repo_path my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 