A **single table** on the input must be specified.

- The data is sent as gzipped csv via the [bulk upload API](https://learn.lytics.com/documentation/developer/api-docs/data-upload#bulk-csv-upload-bulk-csv-upload-post) 
so large volumes are supported. 
- The event stream is automatically created if does not exist.
- Creates / Updates a LQL query based on parameters with alias matching the Stream Name.
- May be executed multiple times

### Dry run

Dry run to verify the data. Only do a dry-run to see if the data is formatted correctly and the query is structured properly. 

**NOTE:**  The data upload and query dry run is executed regardless the `Model -> Load Type` setup. 
When in dry run mode a sample data row is sent to the `query/test` endpoint and the result printed in the 
job log to verify it behaves properly. The job log will contain record like this:

```
Running dry run of the LQL / data upload.
{"_created": "2020-09-22T13:33:43.741754223Z", "_modified": "2020-09-22T13:33:43.741754223Z", "email": "david@keboola.com", "last_order_date": "2020-01-01T00:00:00Z", "name": "keboola.com", "order_value": "150", "test": "a"}
```

### Model Type

Type of the table you want to update. By default `user`, but you may want to update also `content` or other.

### Load Type

The load type defines what actions will be performed on each exectuion:

- **Update Model and Data** - uploads model (LQL query) and loads data each time
- **Update Model Only** - Only updates the LQL query
- **Update Data Only** - Only loads data to the data stream, the LQL query stays intact

#### Model Mapping

Mapping of source column tables to the destination table columns

- **Destination Column** - must not contain blank characters
- **Type** - if set to `Email`, `email()` function will be applied to the field. See below.
- **Is Identifier** - If set to true, the field will be used in the `BY` clause. May be set only on one column.

#### Data Types

Apart from standard primitive datatypes, additional are supported:

- **EMAIL** - `email()` function will be applied to the field
- **EMAIL_DOMAIN** - `emaildomain()` function will be applied to the field

##### **CUSTOM_LQL**

Specify custom LQL (function) in the `Column` field instead of the source table column name. This allows to apply some
 additional LQL functions such as `Map` datatypes. The name of the actual existing column form the source table must be included.
 It is possible to define the KIND as well.
 
 **Examples**:
 
 `map(key1, todate(date_field)) KIND map[string]time` - `date_field` is a column in the input table
 `domain(url)`- `url` is a column in the input table