Write data to custom Lytics streams. Allows sending large volumes of data using gzipped stream and dry run mode 
to verify your data.